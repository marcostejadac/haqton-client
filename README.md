# Haqton br  2020

Repositório do Haqton: desafio de programação da QtCon Brasil 2020

## Getting Started

Haqton challenge for QtCon Brazil 2020

### Prerequisites

- Qt 5.15
- cmake

Android 
- ninja
- ndk (for android development)
- openssl [https://github.com/KDAB/android_openssl](https://github.com/KDAB/android_openssl)

## Deployment

Open CMakeLists.txt with QtCreator and select Kit Platforms to build (Desktop) or Android.


For Desktop app 

```
mkdir build && cd build 
cmake .. 
make
./haqton

```


## Built With

* QtCreator
* [ZeroMQ](https://zeromq.org/)

## Authors

* **Marcos Tejada**
* **Julissa Villanueva** 

## License

This project is licensed under the GPLv3 License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* QtCon br
* Sandro Andrade for share your knowledge

## Resources

* Request controller is based in Emily-Client from Sandro Andrade repo
* Examples QML - Gallery - Maroon in Trouble
